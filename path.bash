#!/usr/bin/env bash
#
# Sourcing this file will export `path` command to the environment as well as
# an assortment of `_path_subcommand`s which can be used separately with the
# same effect as `path subcommand`.

path() {
    local var='PATH'
    local sep=':'
    local var_arr=() # Array of separated items of $VAR
    local print_var=false

    # Parse OPTIONS if present
    for _ in {1..3}; do
        case "$1" in
            --var*)
                var="${1#*=}"
                shift
                ;;
            --sep*)
                sep="${1#*=}"
                shift
                ;;
            -p)
                print_var=true
                shift
                ;;
        esac
    done

    # Parse subcommand (only one is supported atm)
    case "$1" in
        'list')
            _path_list "$var" "$sep"
            ;;
        'add')
            _path_add "$2" "$3" "$var" "$sep"
            ;;
        'prepend')
            _path_add "$2" 0 "$var" "$sep"
            ;;
        'append')
            _path_add "$2" -1 "$var" "$sep"
            ;;
        'clean')
            _path_clean "$var" "$sep"
            ;;
        'rm')
            _path_rm "$2" "$var" "$sep"
            ;;
        'mv')
            _path_mv "$2" "$3" "$var" "$sep"
            ;;
        '')
            print_var=true
            ;;
        *)
            _path_usage
            ;;
    esac

    if $print_var; then
        echo "${!var}"
    fi
}

# Print usage
_path_usage() {
    cat <<EOF
Path manager.

USAGE:
path [OPTIONS] [SUBCOMMAND]
path [--var=VAR] [SUBCOMMAND]

OPTIONS:
--var=VAR     Which environment variable to operate (default is PATH)
--sep=SEP     Character used for separating items in VAR (default is ':')
-p            Print VAR when done with subcommands

SUBCOMMANDS:
<none>        List raw contents of VAR
list          List and enumerate each entry of VAR on a separate line
clean         Remove duplicate and empty entries
append ITEM   Append ITEM to the end of VAR (equal to 'add ITEM -1')
prepend ITEM  Prepend ITEM to the beginning of VAR (equal to 'add ITEM 0')
add ITEM POS  Add ITEM to VAR *before* an entry on position POS. Negative POS
              inserts from the back *after* the existing entry.
rm WHAT       Remove WHAT from VAR. WHAT can be:
                a) an index following the same rules as POS in 'add'
                b) the item itself
              In both cases, all existing entries are removed.
mv ITEM POS   Move ITEM to a position POS. Performs 'rm' followed by 'add'.
EOF
}

# Split variable and print each entry on a new line with its number
# Args (mandatory):
#   $1 variable (e.g. PATH)
#   $2 split character (e.g. ':')
_path_list() {
    if (($# != 2)); then
        echo "'_path_list' requires 2 args. Received: '$@'" >&2
        return 1
    fi

    local var="$1"
    local sep="$2"
    local var_arr=()

    # preserve last empty entry, e.g. "a:b:c:"
    # http://mywiki.wooledge.org/BashPitfalls#pf47
    local IFS=
    IFS=$sep read -r -a var_arr <<< "${!var}$sep"

    local len="${#var_arr[@]}" # Number of entries in $var
    local max_idx=$((len-1))   # Largest possible index (indexing from 0)
    local pad="${#max_idx}"    # Number of chars required to print $max_idx

    for i in "${!var_arr[@]}"; do
        printf "%${pad}s %s\n" $i "${var_arr[i]}"
    done
}

# Insert an entry into a variable on a specified position
# Args (mandatory):
#   $1 item to be inserted
#   $2 position (0: first, 1: second, -1: last, -2: second last, etc.)
#   $3 variable (e.g. PATH)
#   $4 split character (e.g. ':')
_path_add() {
    if (($# != 4)); then
        echo "'_path_add' requires 4 args. Received: '$@'" >&2
        return 1
    fi

    local item="$1"
    local pos="$2"
    local var="$3"
    local sep="$4"
    local var_arr=()
    local re_int='^[+-]?[0-9]+$'

    if ! [[ $pos =~ $re_int ]]; then
        echo "'path add': $pos is not an integer." >&2
        return 1
    fi

    local IFS=
    IFS=$sep read -r -a var_arr <<< "${!var}$sep"

    # pos is the index of an element before which to insert item
    local len="${#var_arr[@]}"
    if ((pos < 0)); then
        pos=$((len+pos+1))
    fi

    var_arr=( "${var_arr[@]:0:$pos}" "$item" "${var_arr[@]:$pos}" )

    local IFS=$sep
    read -r "${var?}" <<< "${var_arr[*]}"
}

# Remove duplicates and empty entries
# Args (mandatory):
#   $1 variable (e.g. PATH)
#   $2 split character (e.g. ':')
_path_clean() {
    if (($# != 2)); then
        echo "'_path_clean' requires 2 args. Received: '$@'" >&2
        return 1
    fi

    local var="$1"
    local sep="$2"
    local var_arr=()
    local new_arr=()
    declare -A seen

    # preserve last empty entry, e.g. "a:b:c:"
    # http://mywiki.wooledge.org/BashPitfalls#pf47
    local IFS=
    IFS=$sep read -r -a var_arr <<< "${!var}$sep"

    local v
    for v in "${var_arr[@]}"; do
        # Remove empty
        if [[ -z $v ]]; then
            continue
        fi

        # Remove duplicate
        if [[ -n ${seen[$v]} ]]; then
            continue
        fi
        seen[$v]=true

        new_arr+=("$v")
    done

    local IFS=$sep
    read -r "${var?}" <<< "${new_arr[*]}"
}

# Remove all occurences of an entry from the variable
# Args (mandatory):
#   $1 item to be removed (either index or the item itself)
#   $2 variable (e.g. PATH)
#   $3 split character (e.g. ':')
_path_rm() {
    if (($# != 3)); then
        echo "'_path_rm' requires 3 args. Received: '$@'" >&2
        return 1
    fi

    local var="$2"
    local sep="$3"
    local re_int='^[+-]?[0-9]+$'
    local var_arr=()
    local new_arr=()

    # preserve last empty entry, e.g. "a:b:c:"
    # http://mywiki.wooledge.org/BashPitfalls#pf47
    local IFS=
    IFS=$sep read -r -a var_arr <<< "${!var}$sep"

    local item=
    if [[ $1 =~ $re_int ]]; then  # $3 is an integer
        item="${var_arr[$1]}"
        if [[ -z $item ]]; then
            echo "Index '$1' not present in '\$$var'." >&2
            return 1
        fi
    else  # $3 is the item
        item="$1"
    fi

    local v
    for v in "${var_arr[@]}"; do
        if [[ $item == $v ]]; then
            continue
        fi
        new_arr+=("$v")
    done

    local IFS=$sep
    read -r "${var?}" <<< "${new_arr[*]}"
}

# Move an entry into a specified position. Implemented as rm + add.
# Args (mandatory):
#   $1 item to be moved
#   $2 final position (0: first, 1: second, -1: last, -2: second last, etc.)
#   $3 variable (e.g. PATH)
#   $4 split character (e.g. ':')
_path_mv() {
    if (($# != 4)); then
        echo "'_path_rm' requires 4 args. Received: '$@'" >&2
        return 1
    fi

    local item="$1"
    local pos="$2"
    local var="$3"
    local sep="$4"
    local var_arr=()

    # preserve last empty entry, e.g. "a:b:c:"
    # http://mywiki.wooledge.org/BashPitfalls#pf47
    local IFS=
    IFS=$sep read -r -a var_arr <<< "${!var}$sep"

    local match=false
    for v in "${var_arr[@]}"; do
        if [[ $item == $v ]]; then
            match=true
            break
        fi
    done

    if $match; then
        _path_rm "$item" "$var" "$sep"
        _path_add "$item" "$pos" "$var" "$sep"
    else
        echo "Item '$item' not found in '\$$var'." >&2
        return 1
    fi
}
